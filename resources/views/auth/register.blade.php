<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500"/>
            </a>
        </x-slot>
        <h2>Форма реєстрації користувача</h2>
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors"/>

        <form method="POST" action="{{ route('register') }}">
        @csrf
        <!--  teacher/student -->
            @if($roles)
                <div class="mt-4 ">
                    <p class="form-check-label">Вчитель чи учень</p>
                    @foreach($roles as $role)
                        <div class="form-check m-2">
                            <input class="form-check-input" id="RadioStudent" type="radio" name="role"
                                   value="{{$role->id}}">
                            <label class="form-check-label" for="RadioStudent">{{$role->description}}</label>
                        </div>
                    @endforeach
                </div>
            @endif

        <!-- Name -->
            <div>
                <x-label for="name" :value="__('Ім\'я')"/>
                <span class="text-danger">*</span>

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required
                         autofocus/>
            </div>
            <!-- Surname -->
            <div>
                <x-label for="surname" :value="__('Прізвище')"/>
                <span class="text-danger">*</span>

                <x-input id="surname" class="block mt-1 w-full" type="text" name="surname" :value="old('surname')"
                         required autofocus/>
            </div>
            <!-- Patronymic -->
            <div>
                <x-label for="patronymic" :value="__('По батькові')"/>
                <span class="text-danger">*</span>

                <x-input id="patronymic" class="block mt-1 w-full" type="text" name="patronymic"
                         :value="old('patronymic')" required autofocus/>
            </div>

            <!-- Email Address -->
            <div class="mt-2">
                <x-label for="email" :value="__('Пошта')"/>
                <span class="text-danger">*</span>

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required/>
            </div>
            <!-- Phone -->
            <div class="mt-2">
                <x-label for="phone" :value="__('Телефон')"/>

                <x-input id="phone" class="block mt-1 w-full" type="tel" name="phone" :value="old('phone')" required/>
            </div>

            <!-- Password -->
            <div class="mt-2">
                <x-label for="password" :value="__('Пароль')"/>
                <span class="text-danger">*</span>

                <x-input id="password" class="block mt-1 w-full"
                         type="password"
                         name="password"
                         :value="old('password')"
                         required autocomplete="new-password"/>
            </div>

            <!-- Confirm Password -->
            <div class="mt-2">
                <x-label for="password_confirmation" :value="__('Повторити пароль')"/>
                <span class="text-danger">*</span>

                <x-input id="password_confirmation" class="block mt-1 w-full"
                         type="password"
                         name="password_confirmation" required/>
            </div>


            <div class="mt-2">
                <span class="text-danger">* - обов'язкові поля для заповнення</span>
            </div>
            <div class="flex items-center justify-end m-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Вже зареєстровані?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Реєстрація') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
