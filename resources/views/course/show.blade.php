@extends('cabinet.sample')
@section('content')


    <div class="container">
        <h2 class="text-center mb-5">Курс {{ $course->name }}</h2>
        <div class="row">
            @if (session()->has('flash_success'))
                <div class="toast-body bg-success text-white">
                    {{ session()->get('flash_success') }}
                </div>
            @endif
            <div class="col-4 p-3 mb-2 bg-secondary text-white">
                <p>Короткий опис курсу: <br>{{ $course->description }}</p>
                <hr>
                <a href="{{ route('course.material.create', $course) }}" class="btn btn-primary">
                    Створити блок теорії
                </a>
                <a href="{{ route('cabinet.index') }}" class="btn btn-primary">
                    Усі мої курси
                </a>
            </div>
            <div class="col">
                <h3 class="text-center mb-2">Теоретичний матеріал</h3>
                <hr>
                @foreach($courseMaterials as $item)
                    <p>Назва блоку: {{ $item->name }}</p>
                    <p>Зміст: {{ $item->description}}</p>
                    <div class="row">
                        <div class="col">
                            <a class="btn btn-success"
                               href="{{route('course.material.edit', [$course, $item])}}">
                                <i class="fa-solid fa-pen-to-square"></i>
                                Змінити</a>
                        </div>
                        <div class="col">
                            <form action="{{route('course.material.destroy', [$course, $item])}}"
                                  method="post">
                                @csrf
                                @method('delete')
                                <button class="ml-3 btn btn-danger" type="submit">
                                    Вилучити
                                </button>
                            </form>
                        </div>
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
@endsection
