@extends('cabinet.sample')
@section('content')
    <div class="container" >
        <h4 class="text-center me-2">Додати блок теорії до цього курсу</h4>
        <form class="w-50" enctype="multipart/form-data" method="post"
              action="{{ route('course.material.store', $course) }}">
            @csrf
            {{--name--}}
            <div class="form-group mb-3">
                <label for="inputName">Назва блоку</label>
                <input name="name" class="form-control @error('name') is-invalid @enderror"
                       id="inputName" type="text"
                       value="{{ old('name')}}"
                       placeholder="Назва блоку">
                @error('name')
                <span class="alert-danger">{{ $message }}</span>
                @enderror
            </div>
            {{-- description--}}
            <div class="form-group mb-3">
                <label for="inputName">Матеріал теоретичного блоку</label>
                <input name="description" class="form-control @error('description') is-invalid @enderror"
                       id="inputDescription" type="text"
                       value="{{ old('description')}}" placeholder="Матеріал теоретичного блоку">
                @error('description')
                <span class="alert-danger">{{ $message }}</span>
                @enderror
            </div>
            <button class="btn btn-primary" type="submit">Додати</button>
        </form>
    </div>
@endsection
