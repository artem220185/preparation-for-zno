<button type="submit" class="btn btn-primary" {{ $attributes->merge(['type'
=> 'submit',
'class' => 'btn']) }}>
    {{ $slot }}
</button>
