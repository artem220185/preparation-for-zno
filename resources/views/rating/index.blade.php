@extends('rating.app')
@section('content')
    <div class="container">
        <a href="/" class="btn btn-warning">На головну</a>

    <h1 class="text-center">Рейтинг учитилей</h1>
    @if (session()->has('flash_success'))
        <div class="toast-body bg-secondary text-center text-white">
            {{ session()->get('flash_success') }}
        </div>
    @endif
    <ul>
        @foreach($teachers as $teacher)
            <li>
                <h4>{{$teacher->name}}: {{ (int)$teacher->rating }}
                    @if($ratingRecord == null)
                        <a href="{{ route('rating-teacher.rate', $teacher)}}"
                           class="btn btn-success">проголосувати</a>
                    @endif
                </h4>
            </li>
        @endforeach
    </ul>
    </div>
@endsection
