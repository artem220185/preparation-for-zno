@extends('rating.app')
@section('content')

    <h2 class="text-center">Змінити профіль</h2>
    <form class="row g-3 text-center" enctype="multipart/form-data" method="post"
          action="{{ route('rating-teacher.save-rate',$teacher) }}">
        @csrf
        <div class="form-floating mt-4 me-2 ">
            <p class="form-group bd-example">Рейтинг: {{ $teacher->name }}</p>
            <div class="btn-toolbar flex-column" role="toolbar" aria-label="Toolbar with button groups">
                <div class="btn-group-center me-2" role="group" aria-label="Second group">
                    <input type="radio" name="rate" value="1" class="btn btn-secondary">1
                    <input type="radio" name="rate" value="2" class="btn btn-secondary">2
                    <input type="radio" name="rate" value="3" class="btn btn-secondary">3
                    <input type="radio" name="rate" value="4" class="btn btn-secondary">4
                    <input type="radio" name="rate" value="5" class="btn btn-secondary">5
                </div>
            </div>


            {{--            @foreach($roles as $role)--}}
            {{--                <div class="form-check">--}}
            {{--                    <input multiple class="form-check-input @error('role') is-invalid @enderror"--}}
            {{--                           type="checkbox" id="roleCheck" name="role[]"--}}
            {{--                           @if(!empty($userRoles) )--}}
            {{--                           @foreach($userRoles as $item)--}}
            {{--                           @if($role->name == $item )--}}
            {{--                           checked--}}
            {{--                           @endif--}}
            {{--                           @endforeach--}}
            {{--                           @endif--}}
            {{--                           value="{{ old('role', $role->name) }}">--}}
            {{--                    <label class="form-check-label" for="roleCheck">--}}
            {{--                        {{ $role->description }}--}}
            {{--                    </label>--}}
            {{--                </div>--}}
            {{--            @endforeach--}}
            {{--            @error('role')--}}
            {{--            <span class="alert-danger">{{ $message }}</span>--}}
            {{--            @enderror--}}
        </div>

        <div class="form-group w-20">
            <button class="btn btn-primary" type="submit">Відправити</button>
        </div>
    </form>



@endsection
