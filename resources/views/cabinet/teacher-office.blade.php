@extends('cabinet.sample')
@section('content')
    <div class="pt-4 pb-1 border-t border-gray-200">
        <div class="mt-3 space-y-1">
            <form class="btn btn-warning" method="POST" action="{{ route('logout') }}">
                @csrf
                <x-responsive-nav-link :href="route('logout')"
                                       onclick="event.preventDefault();
                                        this.closest('form').submit();">
                    {{ __('Вийти') }}
                </x-responsive-nav-link>
            </form>
            <a href="/" class="btn btn-warning">На головну</a>

        </div>
    </div>
    <p>Личный кабинет учителя {{ Auth::user()->name }} </p>

    <div class="container">
        @if (session()->has('flash_success'))
            <div class="toast-body bg-secondary text-white">
                {{ session()->get('flash_success') }}
            </div>
        @endif
        <div class="row">
            <div class="col">
                <h2 class="text-center">Мої курси</h2>
                @foreach($courses as $course)
                    <p>
                        <a href="{{ route('course.show',$course->slug) }}"
                           class="btn-outline-primary">
                            {{$course->name}}
                        </a>

                        <span>на предмет: {{ $course->category->name }}</span>
                    </p>
                    <div class="row w-50">
                        <div class="col">
                            <a href="{{ route('course.edit', $course) }}" class="btn btn-primary">
                                Змінити курс
                            </a>
                        </div>
                        <div class="col">
                            <form action="{{route('course.destroy', $course)}}"
                                  method="post">
                                @csrf
                                @method('delete')
                                <button class="ml-3 btn btn-danger" type="submit">
                                    Вилучити
                                </button>
                            </form>
                        </div>
                    </div>
                    <hr>
                @endforeach
            </div>
            <div class="col-5 bg-secondary text-white">
                @include('cabinet.includes.profile')
            </div>
        </div>
    </div>
@endsection
