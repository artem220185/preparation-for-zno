@extends('cabinet.sample')
@section('content')
    <div class="pt-4 pb-1 border-t border-gray-200">
        <div class="mt-3 space-y-1">
            <form class="btn btn-warning" method="POST" action="{{ route('logout') }}">
                @csrf
                <x-responsive-nav-link :href="route('logout')"
                                       onclick="event.preventDefault();
                                        this.closest('form').submit();">
                    {{ __('Вийти') }}
                </x-responsive-nav-link>
            </form>

            <a href="/" class="btn btn-warning">На головну</a>
        </div>
    </div>
    <p>Личный кабинет ученика {{ Auth::user()->name }} </p>

    <div class="container">
        @if (session()->has('flash_success'))
            <div class="toast-body bg-success text-white">
                {{ session()->get('flash_success') }}
            </div>
        @endif
        <div class="row">
            <div class="col">
                <h2 class="text-center">Вибрані курси</h2></div>
            <div class="col-5 bg-secondary text-white">
                @include('cabinet.includes.profile')

            </div>
        </div>
    </div>
@endsection
