<h2 class="text-center">Профиль</h2>

<ul class="list-group list-group-flush">
    <li class="list-group-item">
        <img style="height: 200px;" src="{{$user->image_url}}" alt="{{ $user->name }}">
    </li>
    <li class="list-group-item">Прізвище: {{$user->surname}}</li>
    <li class="list-group-item">Ім'я: {{$user->name}}</li>
    <li class="list-group-item">По батькові: {{$user->patronymic}}</li>
    <li class="list-group-item">
        @if (Auth::user()->hasRoles('teacher'))
            Місце роботи:
        @else
            Місце навчання:
        @endif {{$user->work_place}}
    </li>
        @if (Auth::user()->hasRoles('student'))
        <li class="list-group-item">
            Класс: {{$user->school_class}}
        </li>
        @endif
    <li class="list-group-item">Телефон: {{$user->phone}}</li>
    <li class="list-group-item">Пошта: {{$user->email}}</li>
    @if (Auth::user()->hasRoles('teacher'))
        <li class="list-group-item">Рейтинг: {{$user->rating}}</li>
    @endif
</ul>
<a href="{{ route('cabinet.edit') }}" class="btn btn-info m-2">Змінити</a>
