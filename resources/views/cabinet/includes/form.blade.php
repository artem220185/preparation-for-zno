

{{-- image--}}
<div class="form-group mb-3">
    <input type="hidden" name="image" placeholder="image">
    <img style="height: 150px;" src="{{$user->image_url}}"
         alt="{{$user->name}}">
</div>
<div class="form-floating">
    <input type="file"  name="image_upload" placeholder="image"
           value="{{ old('image_upload') ?? $user->image}}">
</div>

{{-- name--}}
<div class="form-floating">
    <input name="name" class="form-control @error('name') is-invalid @enderror"
           id="inputName" type="text"
           value="{{ old('name',$user->name)  }}" placeholder="Ім'я">
    <label for="inputName">Ім'я</label>
    @error('name')
    <span class="alert-danger">{{ $message }}</span>
    @enderror
</div>

{{-- surname--}}
<div class="form-floating">
    <input name="surname" class="form-control @error('surname') is-invalid @enderror"
           id="inputSurname" type="text"
           value="{{ old('surname', $user->surname) }}" placeholder="Прізвище'я">
    <label for="inputSurname">Прізвище'я</label>
    @error('surname')
    <span class="alert-danger">{{ $message }}</span>
    @enderror
</div>

{{-- patronymic--}}
<div class="form-floating">
    <input name="patronymic" class="form-control @error('patronymic') is-invalid @enderror"
           id="inputPatronymic"
           value="{{ old('patronymic',$user->patronymic) }}" type="text" placeholder="По батькові'я">
    <label for="inputPatronymic">По батькові'я</label>
    @error('patronymic')
    <span class="alert-danger">{{ $message }}</span>
    @enderror
</div>

{{-- email--}}
<div class="form-floating">
    <input name="email" class="form-control @error('email') is-invalid @enderror"
           id="inputEmail"
           value="{{ old('email',$user->email)}}" type="email" placeholder="Пошта">
    <label for="inputEmail">Пошта</label>
    @error('email')
    <span class="alert-danger">{{ $message }}</span>
    @enderror
</div>

{{-- phone--}}
<div class="form-floating">
    <input name="phone" class="form-control  @error('phone') is-invalid @enderror"
           id="inputPhone"
           value="{{ old('phone', $user->phone ) }}" type="tel" placeholder="Телефон">
    <label for="inputPhone">Телефон</label>
    @error('phone')
    <span class="alert-danger">{{ $message }}</span>
    @enderror
</div>

{{-- work_place--}}
<div class="form-floating">
    <input name="work_place" class="form-control @error('work_place') is-invalid @enderror"
           id="work_placeInput"
           value="{{ old('work_place',$user->work_place) }}" type="text" placeholder="Школа: ">
    <label for="work_placeInput">
        @if (Auth::user()->hasRoles('teacher'))
            Місце роботи:
        @else
            Місце навчання:
        @endif

    </label>
    @error('work_place')
    <span class="alert-danger">{{ $message }}</span>
    @enderror
</div>

@if (Auth::user()->hasRoles('student'))
    {{-- work_place--}}
    <div class="form-floating">
        <input name="school_class" class="form-control @error('school_class') is-invalid @enderror"
               id="school_class"
               value="{{ old('school_class',$user->school_class) }}" type="text" placeholder="Класс:">
        <label for="school_class">Класс:</label>
        @error('school_class')
        <span class="alert-danger">{{ $message }}</span>
        @enderror
    </div>
@endif

@if(empty($user->id))

    <!-- password -->
    <div class="form-floating">
        <input name="password" class="form-control @error('password') is-invalid @enderror"
               id="inputPassword"
               value="{{ old('password') }}" type="password" placeholder="Пароль">
        <label for="inputPassword">Пароль</label>
        @error('password')
        <span class="alert-danger">{{ $message }}</span>
        @enderror
    </div>
    <!-- Confirm Password -->
    <div class="form-floating">
        <input id="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror"
               type="password" placeholder="Подтверждение пароля"
               name="password_confirmation" required/>
        <label for="password_confirmation">Подтверждение пароля</label>
        @error('password_confirmation')
        <span class="alert-danger">{{ $message }}</span>
        @enderror
    </div>
@endif
