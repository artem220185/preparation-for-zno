@extends('cabinet.sample')
@section('content')
<div class="container m-4 ">
    <h2 class="text-center">Змінити профіль</h2>
    <form class="row g-3" enctype="multipart/form-data" method="post" action="{{ route('cabinet.update') }}">
        @csrf
        @method('put')
        @include('cabinet.includes.form')

        {{-- button--}}
        <div class="form-group w-20">
            <button class="btn btn-primary" type="submit">Зберегти</button>
        </div>
    </form>
</div>
@endsection
