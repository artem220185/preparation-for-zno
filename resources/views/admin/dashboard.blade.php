@extends('admin.layouts.app')

@section('content')
    <div class="example">

        <div class="tab-content rounded-bottom">
            <div class="tab-pane p-3 active preview" role="tabpanel" id="preview-595">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Кількість усіх користувачів</th>
                        <th scope="col">Вчителів'я</th>
                        <th scope="col">Учнів</th>
                        <th scope="col">Адміністраторів</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">{{ $users->count() }}</th>
                        <th scope="row">{{ $teachers }}</th>
                        <th scope="row">{{ $students }}</th>
                        <th scope="row">{{ $admins }}</th>
                    </tr>
                    </tbody>
                </table>
{{--                {{$users->links()}}--}}
            </div>

            <div class="tab-pane pt-1" role="tabpanel" id="code-595">
                sdsf
            </div>
        </div>
    </div>
@endsection
