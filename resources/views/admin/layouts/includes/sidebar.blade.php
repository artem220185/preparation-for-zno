<div class="sidebar sidebar-dark sidebar-fixed" id="sidebar">
    <div class="sidebar-brand d-none d-md-flex">
        <a class="nav-link nav-group-toggle text-white" href="/">
            На головну сторінку сайту
        </a>

    </div>
    <ul class="sidebar-nav" data-coreui="navigation" data-simplebar>
        {{--        верхний элемент sidebar--}}
        <li class="nav-item"><a class="nav-link" href="{{ route('admin.dashboard') }}">
                Головна сторінка адмінки</a>
        </li>

        <li class="nav-group">
            <a class="nav-link nav-group-toggle" href="">
                Користувачі
            </a>
            <ul class="nav-group-items">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.user.index', ['role'=>'admin'])}}" target="_top">
                        <i class="fa-solid fa-street-view me-2"></i>
                        Адміністратори
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.user.index',['role'=>'teacher'])}}" target="_top">
                        <i class="fa-solid fa-chalkboard-user me-2"></i>Вчителі
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.user.index',['role'=>'student'])}}" target="_top">
                        <i class="fa-solid fa-graduation-cap me-2"></i>
                        Учні
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.user.index',['role'=>''])}}" target="_top">
                        <i class="fa-solid fa-users me-2"></i>
                        Показати всіх
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.user.create') }}" target="_top">
                        <i class="fas fa-user-plus me-2"></i>
                        Додати користувача
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
