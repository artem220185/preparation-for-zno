@extends('admin.layouts.app')

@section('content')
    <div class="example">
        @if (session()->has('flash_success'))
            <div class="toast-body bg-success text-white">
                {{ session()->get('flash_success') }}
            </div>
        @endif

        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-coreui-toggle="tab" href="#preview-595" role="tab">
                    <svg class="icon me-2">
                        <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-media-play"></use>
                    </svg>
                    Preview</a></li>
            <li class="nav-item"><a class="nav-link" data-coreui-toggle="tab" href="#code-595" role="tab">
                    <svg class="icon me-2">
                        <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-code"></use>
                    </svg>
                    Code</a></li>
        </ul>
        <div class="tab-content rounded-bottom">
            <div class="tab-pane p-3 active preview" role="tabpanel" id="preview-595">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Прізвище</th>
                        <th scope="col">Ім'я</th>
                        <th scope="col">Фото</th>
                        <th scope="col">Статус</th>
                        <th scope="col">Дії</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <th scope="row">{{ $user['id'] }}</th>
                            <td>{{ $user['surname'] }}</td>
                            <td>{{ $user['name'] }}</td>
                            <td>
                                <img style="height: 50px;" src="{{ $user->image_url }}" alt="">
                            </td>
                            <td>
                                @foreach($user->roles as $role)
                                    {{$role['description']}}
                                @endforeach
                            </td>
                            <td>
                                <div class="row justify-content-center">
                                    <div class="col-5">
                                        <a class="col btn btn-success"
                                           href="{{ route('admin.user.edit', $user)}}">
                                            <i class="fa-solid fa-pen-to-square"></i>
                                            Змінити</a>
                                    </div>
                                    <div class="col-5">
                                        <form class="col"
                                              action="{{ route('admin.user.destroy', $user)}}"
                                              method="post">
                                            @csrf
                                            @method('delete')
                                            <button class="ml-3 btn btn-danger" type="submit">
                                                <i class="fa-solid fa-trash-can"></i>
                                                Вилучити
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                {{$users->links()}}
            </div>

            <div class="tab-pane pt-1" role="tabpanel" id="code-595">
                sdsf
            </div>
        </div>
    </div>
@endsection
