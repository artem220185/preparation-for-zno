@extends('admin.layouts.app')

@section('content')

    <form class="row g-3" enctype="multipart/form-data" method="post" action="{{route('admin.user.update',$user)}}">
        @csrf
        @method('put')
        @include('admin.user.includes.form')

        {{-- button--}}
        <div class="form-group w-20">
            <button class="btn btn-primary" type="submit">Зберегти</button>
        </div>
    </form>

@endsection
