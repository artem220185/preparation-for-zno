@extends('admin.layouts.app')

@section('content')
    <form class="row g-3" enctype="multipart/form-data" method="post" action="{{ route('admin.user.store') }}">
        @csrf
        @include('admin.user.includes.form')

        {{-- button--}}
        <div class="form-group w-20">
            <button class="btn btn-primary" type="submit">Додати</button>
        </div>
    </form>
@endsection
