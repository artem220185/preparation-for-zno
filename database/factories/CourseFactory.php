<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CourseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
           'category_id'=>$this->faker->numberBetween(1,8),
            'user_id'=>$this->faker->randomDigit(3,6),
            'name'=>$this->faker->name(),
            'description'=>$this->faker->text(100),
        ];
    }
}
