<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CourseMaterialFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'course_id'=>$this->faker->numberBetween(9,23),
            'name'=>$this->faker->name(),
            'description'=>$this->faker->text(150),
        ];
    }
}
