<?php

namespace App\Http\Controllers\Cabinet;

use App\Models\Course;
use App\Models\CourseMaterial;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CabinetCourseController extends Controller
{
    public function show($slug)
    {
        $user = Auth::user();
        if ($user->hasRoles('teacher')) {

            $course = $user->courses()->where('slug', $slug)->first();
            $courseMaterials = $course->courseMaterials()->where('course_id', $course->id)->get();

            return view('course.show', [
                'course' => $course,
                'courseMaterials' => $courseMaterials
            ]);
        } else {
            return redirect()->route('cabinet.index');
        }
    }

    public function create(Request $request, $slug)
    {
        dd('create');
        return redirect()->route('course-cabinet.show', $slug)->withFlashSuccess("Теоретичний блок $request->name, доданий");
    }

    public function edit(Course $course)
    {
        dd($course);
    }

    public function store(Request $request, $slug)
    {
        if (!empty($request)) {
            $courseId = Course::query()->where('slug', $slug)->first()->id;
            $courseMaterial = new CourseMaterial();
            $courseMaterial->fill($request->all());
            $courseMaterial->course_id = $courseId;
            $courseMaterial->save();
        } else {
            return redirect()->route('course-cabinet.show', $slug);
        }
        return redirect()->route('course-cabinet.show', $slug)->withFlashSuccess("Теоретичний блок $request->name, доданий");
    }

    public function destroy(Course $course)
    {
        dd('destroy');
        $name = $item->name;
        $item->delete();
        return redirect()->route('course-cabinet.show')->withFlashSuccess("Блок теорії  $name, вилучений");
    }
}
