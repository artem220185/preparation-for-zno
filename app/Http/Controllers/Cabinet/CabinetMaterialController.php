<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\CourseMaterial;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CabinetMaterialController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Course $course)
    {
        return view('course.cabinet-material.create', ['course' => $course]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Course $course
     * @return Response
     */
    public function store(Request $request, Course $course)
    {
        $material = new CourseMaterial();
        $material->fill($request->all());
        $material->course_id = $course->id;
        $material->save();

        return redirect()->route('course.show', $course->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param Course $course
     * @param CourseMaterial $material
     * @return Response
     */
    public function show(Course $course, CourseMaterial $material)
    {
        dd('show - CabinetMaterialController');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Course $course
     * @param CourseMaterial $material
     * @return Response
     */
    public function edit(Course $course, CourseMaterial $material)
    {
        return view('course.cabinet-material.edit', [
            'course' => $course,
            'material' => $material
        ]);
//        return redirect()->route('course.show', $course->slug)->withFlashSuccess("Блок $material->name, змінено");

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Course $course
     * @param CourseMaterial $material
     * @return Response
     */
    public function update(Request $request, Course $course, CourseMaterial $material)
    {
        $material->fill($request->all());
        $material->save();
        return redirect()->route('course.show', $course->slug)->withFlashSuccess("Блок $material->name, змінено");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Course $course
     * @param CourseMaterial $material
     * @return Response
     */
    public function destroy(Course $course, CourseMaterial $material)
    {
        $name = $material->name;
        $material->delete();
        return redirect()->route('course.show', $course->slug)->withFlashSuccess("Блок $name, видалено");
    }
}
