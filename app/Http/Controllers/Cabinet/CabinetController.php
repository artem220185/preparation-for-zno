<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CabinetController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if ($user->hasRoles('teacher')) {
            $rating = DB::table('ratings_teachers')->where('teacher_id', $user->id)->avg('rating');
            $courses = $user->courses()->with(['category'])->get();

            return view('cabinet.teacher-office', [
                'user' => $user,
                'rating' => $rating,
                'courses' => $courses,
            ]);
        }
        if ($user->hasRoles('student')) {
            return view('cabinet.student-office', [
                'user' => $user,
            ]);
        }
        if ($user->hasRoles('admin')) {
            return redirect()->route('admin.user.index');
        }
    }

    public function edit()
    {
        return view('cabinet.edit', [
            'user' => Auth::user()
        ]);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $user->fill($request->all());
        if ($request->hasFile('image_upload')) {
            $user->uploadImage($request->file('image_upload'));
        }
        $user->save();
        return redirect()->route('cabinet.index')->withFlashSuccess("Користувач $user->name  змінено");
    }
}
