<?php

namespace App\Http\Controllers;

use App\Models\RatingTeacher;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RatingTeacherController extends Controller
{
    public function index(Request $request)
    {
        $teachers = User::query()->whereHas('roles', fn($q) => $q->where('name', 'teacher'))->paginate(20);

        $ratingRecord = RatingTeacher::where('teacher_id', '=', Auth::id())->first();
        return view('rating.index', [
            'teachers' => $teachers,
            'ratingRecord' => $ratingRecord
        ]);
    }

    public function rate(User $user)
    {
        return view('rating.rate', [
            'teacher' => $user
        ]);
    }

    public function saveRate(Request $request, User $user)
    {
        $ratingRecord = RatingTeacher::where('student_id', Auth::id())->where('teacher_id', $user->id)->first();

        if (empty($ratingRecord)) {
            $ratingRecord = new RatingTeacher();
            $ratingRecord->teacher_id = $user->id;
            $ratingRecord->student_id = Auth::id();
        }
        $ratingRecord->rating = $request->rate;
        $ratingRecord->save();

        return redirect()->route('rating-teacher.index')->withFlashSuccess("Дякуємо за Ваш голос!");
    }
}
