<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::all();
        $t = 0;
        $s = 0;
        $a = 0;

        foreach ($users as $user) {
            foreach ($user->roles as $item) {
                if ($item['name'] == 'teacher') {
                    $t++;
                } elseif ($item['name'] == 'student') {
                    $s++;
                } elseif ($item['name'] == 'admin') {
                    $a++;
                }
            }
        }

        return view('admin.dashboard', [
            'users' => User::query(),
            'teachers' => $t,
            'students' => $s,
            'admins' => $a,
        ]);
    }
}
