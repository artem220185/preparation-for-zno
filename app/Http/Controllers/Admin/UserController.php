<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserStoreRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
        $usersQ = User::query();

        $role = $request->input('role', '');
        if (!empty($role)) {
            $usersQ->whereHas('roles', fn($q) => $q->where('name', $role));
        }

        return view('admin.user.index', [
            'users' => $usersQ->paginate(6),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {

        $user = new User();
        return view('admin.user.create', [
            'user' => $user,
            'roles' => Role::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
dd('store');
//        $user = User::create([
//            'name' => $request->name,
//            'surname' => $request->surname,
//            'patronymic' => $request->patronymic,
//            'email' => $request->email,
//            'phone' => $request->phone,
//            'password' => Hash::make($request->password), // 123456789
//            'email_verified_at' => now(),
//            'remember_token' => Str::random(10),
//        ]);

//        $roleIds = Role::whereIn('name', $request->role)->get()->pluck('id')->toArray();
//        $user->roles()->sync($roleIds);
//        $data = $request->except(['_token', '_method', 'role']);
        $user->fill($request->all());
        if ($request->hasFile('image_upload')) {
            $user->uploadImage($request->file('image_upload'));
        }
        $user->save();


        $roleIds = Role::whereIn('name', $request->role)->get()->pluck('id')->toArray();
        $user->roles()->sync($roleIds);
        return redirect()->route('admin.user.index')->withFlashSuccess("Пользователь $request->name добавлен");
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return View
     */
    public function show(User $user)
    {
        return view('admin.user.show', [
            'user' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return View
     */

    public function edit(User $user)
    {
        $userRoles = $user->roles->pluck('name')->toArray();
        $roles = Role::all();

        return view('admin.user.edit', [
            'user' => $user,
            'roles' => $roles,
            'userRoles' => $userRoles,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        /*
        $user = Auth::user();
        $user->fill($request->all());
        if($request->hasFile('image_upload')){
            $user->uploadImage($request->file('image_upload'));
        }
        $user->save();
        */

        $roleIds = Role::whereIn('name', $request->role)->get()->pluck('id')->toArray();
        $user->roles()->sync($roleIds);
//        $data = $request->except(['_token', '_method', 'role']);
        $user->fill($request->all());
        if ($request->hasFile('image_upload')) {
            $user->uploadImage($request->file('image_upload'));
        }
        $user->save();

        return redirect()->route('admin.user.index')->withFlashSuccess("Користувач $user->name  змінено");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $name = $user->name;
        $user->delete();
        return redirect()->route('admin.user.index')->withFlashSuccess("Пользователь $name  добавлен");
    }
}
