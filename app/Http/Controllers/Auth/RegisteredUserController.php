<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register', [
            'roles' => Role::where('name', '!=', 'admin')->get(),
        ]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'min:3', 'max:100'],
            'surname' => ['required', 'string', 'min:3', 'max:100'],
            'patronymic' => ['required', 'string', 'min:3', 'max:100'],
            'email' => ['required', 'string', 'email', 'unique:users'],
            'phone' => ['string', 'min:10', 'max:13', 'unique:users'],
            'role' => ['required', 'string'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'surname' => $request->surname,
            'patronymic' => $request->patronymic,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
        ]);

        $user->roles()->sync([$request->role]);

        event(new Registered($user));

        Auth::login($user);
        if ($request->role == 2) {
            return redirect()->route('cabinet');
            dd('teacher');
        } elseif ($request->role == 3) {
            dd('student');
        } elseif($request->role == 1) {
            dd('admin');
        }

//        return redirect(RouteServiceProvider::HOME);
    }
}
