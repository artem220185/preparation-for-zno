<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:100'],
            'surname' => ['required', 'string', 'min:3', 'max:100'],
            'patronymic' => ['required', 'string', 'min:3', 'max:100'],
            'email' => ['required', 'string', 'email', 'unique:users',
                Rule::unique('users')->ignore($this->route('email')),
                ],
            'phone' => ['required', 'string', 'min:10', 'max:13', 'unique:users',
                Rule::unique('users')->ignore($this->route('phone')),
                ],
            'role_id' => ['min:1', 'nullable'],
            'password' => ['required', 'confirmed'],
        ];
    }
}
