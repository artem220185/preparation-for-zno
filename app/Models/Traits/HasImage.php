<?php


namespace App\Models\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait HasImage
{
    public function getImageUrlAttribute()
    {
        return !empty($this->image) ? Storage::url($this->image) : '/img/no-image.jpg';
    }

    public function uploadImage(UploadedFile $imageFile) {

        $this->deleteImage();
        $fileName = md5(time()).'.'.$imageFile->getClientOriginalExtension();
        $this->image = $imageFile->storeAs('uploads', $fileName,  'public');
    }

    public function deleteImage()
    {
        if(!empty($this->image) && Storage::exists($this->image)){
            Storage::delete($this->image);
        }
        $this->image = null;
    }
}
