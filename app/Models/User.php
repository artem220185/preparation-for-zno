<?php

namespace App\Models;

use App\Models\Traits\HasImage;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasImage;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'surname',
        'patronymic',
        'email',
        'phone',
        'image',
        'work_place',
        'school_class',
        'rating',
        'password',
        'remember_token',
        'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'image_url',
    ];

    public function courses()
    {
        return $this->hasMany(Course::class);
    }
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role');
    }

    public function students()
    {
        return $this->belongsToMany(User::class, 'ratings_teachers', 'student_id', 'teacher_id');
    }

    public function teachers()
    {
        return $this->belongsToMany(User::class, 'ratings_teachers', 'teacher_id', 'student_id')->withPivot('rating');
    }

    public function hasRoles(...$roles)
    {
        foreach ($roles as $role) {
            if ($this->roles->where('name', '=', $role)->count() > 0) {
                return true;
            }
            return false;
        }
    }

    public function getRatingAttribute()
    {
        return  DB::table('ratings_teachers')->where('teacher_id', $this->id)->avg('rating') ?? 0;
    }

}
