<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Cabinet\CabinetCourseController;
use App\Http\Controllers\Cabinet\CabinetMaterialController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Cabinet\CabinetController;
use App\Http\Controllers\RatingTeacherController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function () {

    // личные кабинеты
    Route::prefix('cabinet')->group(function () {
        Route::get('index', [CabinetController::class, 'index'])->name('cabinet.index');
        Route::get('edit', [CabinetController::class, 'edit'])->name('cabinet.edit');
        Route::put('update', [CabinetController::class, 'update'])->name('cabinet.update');

        //    курсы
        Route::resource('course', CabinetCourseController::class)->only(['show', 'edit', 'create', 'store', 'destroy']);

        //      теор.материал для курса
        Route::resource('course.material', CabinetMaterialController::class)->only(['show', 'update', 'edit', 'create', 'store', 'destroy']);
    });

        //        рейтинг учителя
    Route::prefix('rating-teacher')->group(function () {
        Route::get('', [RatingTeacherController::class, 'index'])->name('rating-teacher.index');
        Route::get('{user}/rate', [RatingTeacherController::class, 'rate'])->name('rating-teacher.rate');
        Route::post('{user}/rate', [RatingTeacherController::class, 'saveRate'])->name('rating-teacher.save-rate');
    });
});


require __DIR__ . '/auth.php';

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
], function () {
    Route::get('', [DashboardController::class, 'index'])->name('dashboard');

    Route::resource('user', UserController::class);
});


